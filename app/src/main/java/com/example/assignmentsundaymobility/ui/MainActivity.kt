package com.example.assignmentsundaymobility.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.assignmentsundaymobility.R
import com.example.assignmentsundaymobility.ui.home.HomeFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        replaceFragment(HomeFragment(), null)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    public fun replaceFragment(fragment: Fragment, previousFragmentName: String?) {
        if (previousFragmentName == null)
            supportFragmentManager.beginTransaction().replace(R.id.main_activity_container, fragment).commit()
        else
            supportFragmentManager.beginTransaction().replace(
                R.id.main_activity_container,
                fragment,
                fragment::class.java.name
            ).addToBackStack(previousFragmentName).commit()
    }
}
