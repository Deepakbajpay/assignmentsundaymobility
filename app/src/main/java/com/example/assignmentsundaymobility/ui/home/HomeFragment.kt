package com.example.assignmentsundaymobility.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.assignmentsundaymobility.R
import com.example.assignmentsundaymobility.remote.data.UserItem
import com.example.assignmentsundaymobility.ui.BaseFragment
import com.example.assignmentsundaymobility.ui.add_new_user.AddUserFragment
import com.example.assignmentsundaymobility.ui.gallery.PhotoGalleryActivity
import com.example.assignmentsundaymobility.ui.gallery.PhotoGalleryActivity.Companion.ARG_PHOTO_POSITION
import com.example.assignmentsundaymobility.ui.gallery.PhotoGalleryActivity.Companion.DELETE_ITEM_CODE

class HomeFragment : BaseFragment(), HomeContractor.view, MyCustomAdapter.CustomAdapterClickListener,
    AddUserFragment.AddUserCallback {

    private val IMAGE_VIEW_PAGE: Int = 100;
    lateinit var recyclerView: RecyclerView;

    lateinit var adapter: MyCustomAdapter
    lateinit var userITemList: ArrayList<UserItem>
    lateinit var presenter: HomeContractor.presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userITemList = ArrayList()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.home_fragment, container, false);

        presenter = HomePresenterImpl(this, HomeInterceptorImpl())
        recyclerView = view.findViewById(R.id.recycler_view)
        adapter = MyCustomAdapter(userITemList, this)
        val gridLayoutManager = GridLayoutManager(requireContext(), 2)
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = adapter

        if (userITemList.isEmpty())
            presenter.getUsers()
        return view;
    }

    override fun hideLoading() {

    }

    override fun updateListItems(listItems: List<UserItem>) {
        userITemList.clear()
        userITemList.addAll(listItems)
        adapter.notifyDataSetChanged()
    }

    override fun showLoading(showLoading: Boolean) {

    }

    override fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, LENGTH_SHORT).show()
    }

    override fun onImageClick(position: Int) {

        val intent = Intent(activity, PhotoGalleryActivity::class.java)
        intent.putExtra(PhotoGalleryActivity.ARG_PHOTO_URL, userITemList.get(position).getAvatarUrl())
        intent.putExtra(PhotoGalleryActivity.ARG_NAME, userITemList.get(position).getLogin())
        intent.putExtra(PhotoGalleryActivity.ARG_PHOTO_POSITION, position)
        startActivityForResult(intent, IMAGE_VIEW_PAGE)
    }

    override fun onAddItemClick() {
        getMainActivity().replaceFragment(AddUserFragment(this), HomeFragment::class.java.name)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == IMAGE_VIEW_PAGE) {
            if (resultCode == DELETE_ITEM_CODE) {
                if (data != null) {
                    val position = data.getIntExtra(ARG_PHOTO_POSITION, -1);
                    if (position != -1) {
                        userITemList.removeAt(position)
                        adapter.notifyItemRemoved(position)
                    }
                }
            }
        }
    }

    override fun onUserAdded(userItem: UserItem) {
        userITemList.add(userItem)
        adapter.notifyItemInserted(userITemList.size)
    }
}
