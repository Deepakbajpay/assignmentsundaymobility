package com.example.assignmentsundaymobility.ui.gallery

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.assignmentsundaymobility.R
import com.example.assignmentsundaymobility.ui.utils.Utilities

class PhotoGalleryActivity : AppCompatActivity(), View.OnClickListener, Utilities.OnAlertDialogButtonClicked {

    private lateinit var photoUrl: String
    private lateinit var name: String
    private var position: Int = 0
    private lateinit var headerTv: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo_gallery)

        headerTv = findViewById(R.id.header_tv)
        findViewById<ImageView>(R.id.delete_iv).setOnClickListener(this)
        findViewById<ImageView>(R.id.back_iv).setOnClickListener(this)

        if (intent.getStringExtra(ARG_PHOTO_URL) != null) {
            photoUrl = intent.getStringExtra(ARG_PHOTO_URL)!!
            name = intent.getStringExtra(ARG_NAME)!!

            headerTv.setText(name)
            position = intent.getIntExtra(ARG_PHOTO_POSITION, 0)
            supportFragmentManager.beginTransaction().add(
                R.id.photo_gallery_fragment_holder,
                PhotoGalleryPagerFragment.newInstance(photoUrl)
            ).commit()
        } else {
            Toast.makeText(this, "Invalid Gallery", Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    override fun onPositiveButtonClick(view: View) {
        if (view.id == R.id.delete_iv) {
            intent = Intent();
            intent.putExtra(ARG_PHOTO_POSITION, position)
            setResult(DELETE_ITEM_CODE, intent)
            finish()
        }
    }

    override fun onNegativeButtonClick(view: View) {

    }

    override fun onClick(view: View?) {
        if (view!!.id == R.id.delete_iv) {
            Utilities.showAlert(
                this,
                view,
                "Confirm Delete",
                "Are you sure you want to delete this item?",
                "Yes",
                "Cancel",
                this
            )
        } else if (view.id == R.id.back_iv) {
            finish()
        }
    }

    companion object {

        public val DELETE_ITEM_CODE: Int = 101;
        val ARG_PHOTO_URL = "photo_urls"
        val ARG_NAME = "name"
        val ARG_PHOTO_POSITION = "photo_position"
    }
}
