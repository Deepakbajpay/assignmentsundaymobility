package com.example.assignmentsundaymobility.ui.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.assignmentsundaymobility.R
import com.example.assignmentsundaymobility.ui.utils.Utilities
import com.github.chrisbanes.photoview.PhotoView

class PhotoGalleryPagerFragment : Fragment() {
    internal lateinit var photoView: PhotoView
    private var photoUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null) {
            photoUrl = arguments!!.getString(TAG_PHOTO_URL)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_photo_gallery_pager, container, false)
        photoView = rootView.findViewById(R.id.photo_view)
        Utilities.setNormalImage(photoUrl!!, photoView)
        return rootView
    }

    companion object {

        private val TAG_PHOTO_URL = "photo_url_tag"

        fun newInstance(photoUrl: String): PhotoGalleryPagerFragment {

            val args = Bundle()
            args.putString(TAG_PHOTO_URL, photoUrl)
            val fragment = PhotoGalleryPagerFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
