package com.example.assignmentsundaymobility.ui

import android.content.Context
import androidx.fragment.app.Fragment

open class BaseFragment : Fragment() {
    internal lateinit var activity: MainActivity


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivity) {
            this.activity = context as MainActivity
        }
    }

    protected fun getMainActivity(): MainActivity {
        return activity
    }

}