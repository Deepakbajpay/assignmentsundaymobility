package com.example.assignmentsundaymobility.ui.utils

import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import com.example.assignmentsundaymobility.R
import com.squareup.picasso.Picasso
import java.io.File

class Utilities {
    companion object {
        fun setNormalImage(imageUrl: String?, imageView: ImageView) {
            if (imageUrl?.contains("http")!!) {
                Picasso.get().load(imageUrl).into(imageView)
            } else
                Picasso.get().load(File(imageUrl)).into(imageView)
        }

        fun setNormalImage(imageUrl: Int, imageView: ImageView) {
            Picasso.get().load(imageUrl).placeholder(R.drawable.ic_add).into(imageView)
        }


        fun showAlert(
            context: Context,
            view: View,
            title: String,
            message: String,
            positiveButtonText: String,
            negativeButtonText: String,
            clickCallback: OnAlertDialogButtonClicked
        ) {
            AlertDialog.Builder(context).setTitle(title)
                .setMessage(message)
                .setPositiveButton(positiveButtonText) { dialog, which -> clickCallback.onPositiveButtonClick(view) }
                .setNegativeButton(negativeButtonText) { dialog, which -> clickCallback.onNegativeButtonClick(view) }
                .show()
        }

        fun hideKeyboard(view: View?, context: Context) {
            if (view != null) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }

    }


    interface OnAlertDialogButtonClicked {

        fun onPositiveButtonClick(view: View)

        fun onNegativeButtonClick(view: View)
    }
}