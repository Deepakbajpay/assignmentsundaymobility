package com.example.assignmentsundaymobility.ui.add_new_user

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.assignmentsundaymobility.R
import com.example.assignmentsundaymobility.remote.data.UserItem
import com.example.assignmentsundaymobility.ui.BaseFragment
import com.example.assignmentsundaymobility.ui.utils.Utilities
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst

class AddUserFragment(var callback: AddUserCallback) : BaseFragment(), View.OnClickListener {

    private val STORAGE_PERMISSION_REQUEST_CODE: Int = 102
    private lateinit var nameEt: EditText
    private lateinit var userTypeEt: EditText
    private lateinit var userImageIv: ImageView
    private var photopath: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.add_new_user_fragment, container, false);
        nameEt = view.findViewById(R.id.add_name_et)
        userTypeEt = view.findViewById(R.id.add_user_type_et)
        userImageIv = view.findViewById(R.id.user_image)
        view.findViewById<Button>(R.id.add_user_image_picker_bt).setOnClickListener(this)
        view.findViewById<Button>(R.id.submit_user_bt).setOnClickListener(this)

        return view;
    }

    override fun onClick(v: View?) {
        Utilities.hideKeyboard(v, requireContext())

        if (v!!.id == R.id.add_user_image_picker_bt) {
            checkStoragePermission()
        } else if (v.id == R.id.submit_user_bt) {
            val name = nameEt.text.toString().trim()
            val userType = userTypeEt.text.toString().trim()
            if (name.isEmpty()) {
                showMessage("Please enter name")
                nameEt.requestFocus()
            } else if (userType.isEmpty()) {
                showMessage("Please enter user type")
                userTypeEt.requestFocus()
            } else if (photopath == null) {
                showMessage("Please select image")
            } else {
                val userItem = UserItem();
                userItem.setLogin(name)
                userItem.setType(userType)
                userItem.setAvatarUrl(photopath!!)
                callback.onUserAdded(userItem)
                activity.onBackPressed()
            }
        }
    }

    private fun checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), STORAGE_PERMISSION_REQUEST_CODE)
        } else
            openGalleryPicker()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.size > 0) {
                if (grantResults.get(0) == PackageManager.PERMISSION_GRANTED) {
                    openGalleryPicker()
                }
            } else
                showMessage("Permission required")
        }
    }

    private fun showMessage(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show();
    }

    private fun openGalleryPicker() {
        FilePickerBuilder.Companion.instance.setMaxCount(1).enableCameraSupport(false).pickPhoto(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == FilePickerConst.REQUEST_CODE_PHOTO) {
            val photopaths: ArrayList<String>? = data?.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA)
            if (photopaths != null && photopaths.size > 0) {
                photopath = photopaths.get(0)
                if (photopath != null) {
                    Utilities.setNormalImage(photopath, userImageIv)
                }
            }

        }
    }

    public interface AddUserCallback {
        fun onUserAdded(userItem: UserItem)
    }
}