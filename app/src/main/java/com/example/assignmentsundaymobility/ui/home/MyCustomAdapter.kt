package com.example.assignmentsundaymobility.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.assignmentsundaymobility.R
import com.example.assignmentsundaymobility.remote.data.UserItem
import com.example.assignmentsundaymobility.ui.utils.Utilities

class MyCustomAdapter(val userItemList: List<UserItem>, var clickListener: CustomAdapterClickListener) :
    RecyclerView.Adapter<MyCustomAdapter.CustomViewHolder>() {
    private lateinit var context: Context;

    class CustomViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var userImage: ImageView;
        var userName: TextView
        var userType: TextView

        init {
            userImage = itemView.findViewById(R.id.profile_iv)
            userName = itemView.findViewById(R.id.name_tv)
            userType = itemView.findViewById(R.id.user_type)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyCustomAdapter.CustomViewHolder {

        context = parent.context
        return CustomViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_item, parent, false))
    }

    override fun getItemCount(): Int {
        return userItemList.size + 1
    }

    override fun onBindViewHolder(holder: MyCustomAdapter.CustomViewHolder, position: Int) {
        if (position < userItemList.size) {
            val currentItem: UserItem = userItemList.get(position)
            holder.userType.setText(currentItem.getType())
            holder.userName.setText(currentItem.getLogin())
            Utilities.setNormalImage(currentItem.getAvatarUrl(), holder.userImage)

            holder.userImage.setOnClickListener(View.OnClickListener { clickListener.onImageClick(position) })
        } else if (position >= userItemList.size) {
            Utilities.setNormalImage(R.drawable.ic_add, holder.userImage)

            holder.userType.setText("")
            holder.userName.setText("Add New User")
            holder.userImage.setOnClickListener(View.OnClickListener { clickListener.onAddItemClick() })
        }
    }

    public interface CustomAdapterClickListener {
        fun onImageClick(position: Int)
        fun onAddItemClick();
    }
}