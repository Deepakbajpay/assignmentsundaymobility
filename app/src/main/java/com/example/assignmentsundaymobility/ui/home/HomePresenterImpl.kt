package com.example.assignmentsundaymobility.ui.home

import com.example.assignmentsundaymobility.remote.data.UserItem

class HomePresenterImpl(val view: HomeContractor.view, val interceptor: HomeContractor.interceptor) :
    HomeContractor.presenter,
    HomeContractor.interceptor.OnFinish {

    override fun onFailure(message: String) {
        view.showMessage(message)
    }

    override fun onCompleted(listItems: List<UserItem>) {
        view.updateListItems(listItems)
    }

    override fun getUsers() {
        interceptor.getUsersFromNetwork(this)
    }
}