package com.example.assignmentsundaymobility.ui.home

import com.example.assignmentsundaymobility.remote.APIClient
import com.example.assignmentsundaymobility.remote.data.UserItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeInterceptorImpl : HomeContractor.interceptor {

    override fun getUsersFromNetwork(finishedListener: HomeContractor.interceptor.OnFinish) {
        APIClient().getWebServices().getUsers().enqueue(object : Callback<List<UserItem>> {
            override fun onResponse(call: Call<List<UserItem>>, response: Response<List<UserItem>>) {
                println("response " + response)
                if (response.code() == 200) {
                    response.body()?.let { finishedListener.onCompleted(it) }
                } else {
                    finishedListener.onFailure(response.message())
                }
            }

            override fun onFailure(call: Call<List<UserItem>>, t: Throwable) {


                finishedListener.onFailure(t.localizedMessage)
                println("onFailure " +call.request().url() +" "+ t.localizedMessage)
            }

        })
    }
}