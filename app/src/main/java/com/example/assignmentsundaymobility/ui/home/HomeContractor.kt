package com.example.assignmentsundaymobility.ui.home

import com.example.assignmentsundaymobility.remote.data.UserItem

public interface HomeContractor {

    public interface view {
        fun updateListItems(listItems: List<UserItem>)
        fun showLoading(showLoading: Boolean)
        fun showMessage(message: String)
        fun  hideLoading()
    }

    interface interceptor {

        fun getUsersFromNetwork(finishedListener: OnFinish)

        interface OnFinish {
            fun onCompleted(listItems: List<UserItem>)
            fun onFailure(message: String)
        }
    }

    interface presenter {

        fun getUsers()
    }
}