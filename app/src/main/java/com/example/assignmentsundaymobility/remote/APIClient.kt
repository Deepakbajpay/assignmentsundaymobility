package com.example.assignmentsundaymobility.remote

import com.example.assignmentsundaymobility.ui.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Mehul on 11-01-2017.
 */
class APIClient {
    private lateinit var webServices: WebServices

    fun getWebServices(): WebServices {
        webServices = client.create<WebServices>(WebServices::class.java!!)
        return webServices
    }

    companion object {
        val BASE_URL = Constants.BASE_URL
        private lateinit var retrofit: Retrofit

        val client: Retrofit
            get() {

                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient).addConverterFactory(GsonConverterFactory.create())
                    .build()

                return retrofit
            }

        private// Request customization: add request headers
        // <-- this is the important line
        val okHttpClient: OkHttpClient
            get() {

                val builder = OkHttpClient.Builder()
                    .retryOnConnectionFailure(true)

                return builder.readTimeout(30000, TimeUnit.SECONDS)
                    .connectTimeout(30000, TimeUnit.SECONDS)
                    .build()

            }
    }
}
