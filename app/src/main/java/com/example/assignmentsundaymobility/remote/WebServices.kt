package com.example.assignmentsundaymobility.remote

import com.example.assignmentsundaymobility.remote.data.UserItem
import retrofit2.Call
import retrofit2.http.GET

interface WebServices {

    @GET("users")
    fun getUsers(): Call<List<UserItem>>
}
